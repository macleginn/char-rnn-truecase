from subprocess import Popen, PIPE

f = open('output1M.txt', 'w', encoding = 'utf-8')
f.close()

i = 1
outlines = []

with open('data/leipzig1M/test.lower.txt', 'r', encoding = 'utf-8') as inp:
    for line in inp:
#        print(line.encode('unicode-escape'))
        p = Popen(['bash', 'test.sh', 'leipzig1M', 'lstm', 'large', '0'], stdin=PIPE, stdout=PIPE, stderr=PIPE)
        output, err = p.communicate(input = line.encode())
        if err != b'':
            res = line
        else:
            res = output.decode()
#        print(res.encode('unicode-escape'))
#        print()
        outlines.append(res)
        if i % 100 == 0:
            with open('output1M.txt', 'a', encoding = 'utf-8') as out:
                out.writelines(outlines)
            outlines = []
#       if i == 20:
#            break
        i += 1
            
                    
